#![warn(clippy::all, clippy::pedantic)]

pub mod certificate;
pub mod parser;
pub mod protocol;
pub mod request;
